require 'spec_helper'

RSpec.shared_examples 'unauthorized' do |url|
  it 'unauthorized' do
    get url
    expect(last_response.status).to eq 401
  end
end

RSpec.shared_examples 'authorized' do |url|
  it 'authorized' do
    get url
    expect(last_response.status).to eq 200
  end
end