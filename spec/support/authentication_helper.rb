module AuthenticationHelper

  def sign_in(user = nil)
    user ||= FactoryGirl.create(:user)
    token = Knock::AuthToken.new(payload: { sub: user.id }).token
    header "Authorization", "JWT #{token}"
  end

end