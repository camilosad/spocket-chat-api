require 'rails_helper'

RSpec.describe User, type: :model do

  context "relationships" do
    it { should have_many(:received_messages) }
    it { should have_many(:sent_messages) }
  end

  context "validations" do
    it { should validate_presence_of(:email) }
  end

end
