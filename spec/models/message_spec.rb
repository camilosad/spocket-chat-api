require 'rails_helper'

RSpec.describe Message, type: :model do

context "relationships" do
    it { should belong_to(:sender) }
    it { should belong_to(:receiver) }
  end

  context "validations" do
    it { should validate_presence_of(:content) }
  end

  context "delegations" do
    it { should delegate_method(:name).to(:sender).with_prefix }
    it { should delegate_method(:email).to(:sender).with_prefix }
    it { should delegate_method(:name).to(:receiver).with_prefix }
    it { should delegate_method(:email).to(:receiver).with_prefix }
  end

end
