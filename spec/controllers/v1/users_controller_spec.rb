require 'spec_helper'
require 'rails_helper'

describe 'V1::UsersController', type: :api do

  context 'requesting without token' do
    it_behaves_like 'unauthorized', 'v1/users'
  end

  context 'requesting with valid token' do
    before do
      FactoryGirl.create(:user, email: 'user@email.com')
      sign_in
    end

    it_behaves_like 'authorized', 'v1/users'

    it 'returns all users except the requester' do
      get 'v1/users'
      expect(json.count).to eq 1
      expect(json.first['email']).to eq 'user@email.com'
    end
  end

end