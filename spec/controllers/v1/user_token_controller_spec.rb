require 'spec_helper'
require 'rails_helper'

describe 'V1::UserTokenController', type: :api do

  def auth(email, password)
    { 'auth': { 'email': email, 'password': password } }
  end

  context 'requesting with invalid user' do
    it 'return not found' do
      post '/v1/user_token', auth('foo@bar.com','123' )
      expect(last_response.status).to eq 404
    end
  end

  context 'requesting with valid user' do
    before { FactoryGirl.create(:user, email: 'user@email.com', password: '123456') }

    it 'return token' do
      post '/v1/user_token', auth('user@email.com', '123456')
      expect(last_response.status).to eq 201
      expect(json['jwt']).to be_present
    end
  end

end