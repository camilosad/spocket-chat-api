require 'spec_helper'
require 'rails_helper'

describe 'V1::MessagesController', type: :api do

  def message(receiver_id)
    { message: { receiver_id: receiver_id, content: 'hi' } }
  end

  context 'get messages' do
    context 'without token' do
      it_behaves_like 'unauthorized', 'v1/messages'
    end

    context 'with token' do
      before do
        @user1 = FactoryGirl.create(:user, email: 'user1@email.com')
        @user2 = FactoryGirl.create(:user, email: 'user2@email.com')
        @user3 = FactoryGirl.create(:user, email: 'user3@email.com')
        FactoryGirl.create(:message, sender: @user1, receiver: @user2 )
        FactoryGirl.create(:message, sender: @user2, receiver: @user1 )
        FactoryGirl.create(:message, sender: @user2, receiver: @user3 )
        sign_in(@user1)
      end

      it_behaves_like 'authorized', 'v1/messages'

      it 'returns only messages related to requester' do
        get 'v1/messages'
        expect(json.count).to eq 2
      end
    end
  end

  context 'create message' do
    context 'without token' do
      it 'is not authorized' do
        post '/v1/messages', message(1)
        expect(last_response.status).to eq 401
      end
    end

    context 'with token' do
      before do
        @user = FactoryGirl.create(:user)
        sign_in(@user)
      end

      it 'created successfully' do
        post '/v1/messages', message(User.first.id)
        expect(last_response.status).to eq 201
        expect(json['sender_email']).to eq @user.email
      end
    end
  end

  context 'destroy message' do
    before do
      @user1 = FactoryGirl.create(:user, email: 'user1@email.com')
      @user2 = FactoryGirl.create(:user, email: 'user2@email.com')
      @msg = FactoryGirl.create(:message, sender: @user1, receiver: @user2)
    end

    context 'without token' do
      it 'is not authorized' do
        delete "/v1/messages/#{@msg.id}"
        expect(last_response.status).to eq 401
      end
    end

    context 'with token' do
      before { sign_in }

      context "with existing id" do
        it 'deleted successfully' do
          delete "/v1/messages/#{@msg.id}"
          expect(last_response.status).to eq 204
        end
      end

      context "with invalid id" do
        it 'return not found' do
          delete "/v1/messages/abc"
          expect(last_response.status).to eq 404
        end
      end

    end
  end


end