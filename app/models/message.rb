class Message < ApplicationRecord

  belongs_to :sender, class_name: 'User'
  belongs_to :receiver, class_name: 'User'

  validates :content, presence: true

  scope :sent_by, -> (sender_id) { where sender_id: sender_id }
  scope :received_by, -> (receiver_id) { where receiver_id: receiver_id }
  scope :by_user, -> (user_id) { sent_by(user_id).or(self.received_by(user_id)) }

  delegate :name, :email, to: :sender, prefix: true
  delegate :name, :email, to: :receiver, prefix: true

  after_create :broadcast

  def broadcast
    ActionCable.server.broadcast "chat_#{receiver_email}", serialized
  end

  def serialized
    MessageSerializer.new(self).as_json
  end

end
