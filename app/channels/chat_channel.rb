class ChatChannel < ApplicationCable::Channel

  def subscribed
    stream_from chat_channel_name
  end

  def receive(data)
    ActionCable.server.broadcast chat_channel_name, data
  end

  private

  def chat_channel_name
    "chat_#{params[:user]}"
  end

end