class MessageSerializer < ActiveModel::Serializer
  attributes :sender_email, :receiver_email, :content, :sent_at

  def sent_at
    object.created_at.to_formatted_s(:long)
  end
end
