module V1
  class MessagesController < ApplicationController
    before_action :authenticate_user
    rescue_from ActiveRecord::RecordNotFound, with: :head_not_found

    def index
      render json: messages, status: :ok
    end

    def create
      @message = Message.new(message_params)
      @message.sender = current_user

      if @message.save
        render json: @message, status: :created
      else
        render json: { errors: @message.errors }, status: :unprocessable_entity
      end
    end

    def destroy
      message.destroy
      head :no_content
    end

    private

    def head_not_found
      head :not_found
    end

    def messages
      @messages ||= Message.by_user(current_user.id)
                            .includes(:sender, :receiver)
                            .order(:created_at)
    end

    def message
      @message ||= Message.find(params[:id])
    end

    def message_params
      params.require(:message).permit([:receiver_id, :content])
    end
  end
end
