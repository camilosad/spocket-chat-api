module V1
  class UsersController < ApplicationController
    before_action :authenticate_user

    def index
      render json: users, status: :ok
    end

    private

    def users
      @users ||= User.all.reject{ |user| user.id == current_user.id }
    end
  end
end
