Rails.application.routes.draw do

  mount ActionCable.server => '/cable'

  namespace :v1 do
    post 'user_token' => 'user_token#create'

    get :users, to: 'users#index'
    resources :messages, only: [:index, :create, :destroy]
  end

end
