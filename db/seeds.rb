# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.destroy_all
camilo = User.create(name: 'Camilo Sad', email: 'sadcamilo@gmail.com', password: 'spocket')
saba = User.create(name: 'Saba Mohebpour', email: 'saba@spocket.co', password: 'spocket')
tom = User.create(name: 'Tom Hansen', email: 'tom@spocket.co', password: 'spocket')

Message.destroy_all
Message.create(sender: camilo, receiver: saba, content: "what's up man")
Message.create(sender: saba, receiver: camilo, content: "hey dude")
Message.create(sender: camilo, receiver: saba, content: "all good?")
Message.create(sender: saba, receiver: camilo, content: "yeah, yourself?")
Message.create(sender: camilo, receiver: saba, content: "I'm fine, excited to move to Canada!")

Message.create(sender: camilo, receiver: tom, content: "yo dude, how are u?")
Message.create(sender: tom, receiver: camilo, content: "hey, I'm fine, how u doing?")
Message.create(sender: camilo, receiver: tom, content: "doing well man")
Message.create(sender: tom, receiver: camilo, content: "good! are u ready to start with us?")
Message.create(sender: camilo, receiver: tom, content: "sure I am!")